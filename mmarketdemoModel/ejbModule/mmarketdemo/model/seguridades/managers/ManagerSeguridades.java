package mmarketdemo.model.seguridades.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import mmarketdemo.model.auditoria.managers.ManagerAuditoria;
import mmarketdemo.model.core.entities.SegAsignacion;
import mmarketdemo.model.core.entities.SegModulo;
import mmarketdemo.model.core.entities.SegPerfil;
import mmarketdemo.model.core.entities.SegUsuario;
import mmarketdemo.model.core.managers.ManagerDAO;
import mmarketdemo.model.seguridades.dtos.LoginDTO;

/**
 * Implementa la logica de manejo de usuarios y autenticacion.
 * Funcionalidades principales:
 * <ul>
 * 	<li>Verificacion de credenciales de usuario.</li>
 *  <li>Asignacion de modulos a un usuario.</li>
 * </ul>
 * @author mrea
 */
@Stateless
@LocalBean
public class ManagerSeguridades {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
    /**
     * Default constructor. 
     */
    public ManagerSeguridades() {
        
    }
    /**
     * Funcion de inicializacion de datos de usuarios.
     */
    public String inicializarDemo() {
    	mAuditoria.mostrarLog(getClass(), "inicializarDemo", "Inicializacion de bdd demo.");
    	//StoredProcedureQuery query=mDAO.getEntityManager().createNamedStoredProcedureQuery("inicializacion");
    	//query.execute();
    	mDAO.getEntityManager().createNativeQuery("CALL inicializacion()").executeUpdate();
    	mAuditoria.mostrarLog(getClass(), "inicializarDemo", "Perfil admin@mmarketdemo.com reseteado.");
    	mAuditoria.mostrarLog(getClass(), "inicializarDemo", "Asignación de permisos a admin@mmarketdemo.com realizada.");
    	mAuditoria.mostrarLog(getClass(), "inicializarDemo", "Inicializacion de bdd demo terminada.");
		return "Inicialización de bdd demo terminada. Usuario admin@mmarketdemo.com/admin";
    }
    
    
    @SuppressWarnings("unchecked")
	public SegUsuario findSegUsuarioByCorreoClave(String correo,String clave) {
    	List<SegUsuario> lista = mDAO.findWhere(SegUsuario.class, "o.correo='"+correo+"' and o.clave='"+clave+"'", null);
    	if(lista.size()==0)
    		return null;
    	return lista.get(0);
    }
    
    @SuppressWarnings("unchecked")
	public SegUsuario findSegUsuarioByCorreo(String correo) {
    	List<SegUsuario> lista = mDAO.findWhere(SegUsuario.class, "o.correo='"+correo+"'", null);
    	if(lista.size()==0)
    		return null;
    	return lista.get(0);
    }
    
    /**
     * Funcion de autenticacion mediante el uso de credenciales.
     * @param correo El correo del usuario que desea autenticarse.
     * @param clave La contrasenia.
     * @param direccionIP La dirección IP V4 del cliente web.
     * @return La ruta de acceso al sistema.
     * @throws Exception
     */
    public LoginDTO login(String correo,String clave,String direccionIP) throws Exception{
    	//crear DTO:
		LoginDTO loginDTO=new LoginDTO();
		loginDTO.setCorreo(correo);
		loginDTO.setDireccionIP(direccionIP);
		
    	SegUsuario usuario=findSegUsuarioByCorreoClave(correo,clave);
    	if(usuario==null) {
    		mAuditoria.mostrarLog(getClass(), "login", "No coincide usuario/clave "+correo);
    		throw new Exception("Error en credenciales.");
    	}
    		
		if(usuario.getActivo()==false) {
    		mAuditoria.mostrarLog(getClass(), "login", "Intento de login usuario desactivado "+correo);
    		throw new Exception("El usuario esta desactivado.");
    	}
		mAuditoria.mostrarLog(loginDTO, getClass(), "login", "Login exitoso "+correo);
		
		//loginDTO.setCorreo(usuario.getCorreo());
		loginDTO.setIdSegUsuario(usuario.getIdSegUsuario());
		//aqui puede realizarse el envio de correo de notificacion.
		
		//obtener la lista de perfiles a los que tiene acceso:
		List<SegAsignacion> listaAsignaciones=findAsignacionByUsuario(usuario.getIdSegUsuario());
		for(SegAsignacion asig:listaAsignaciones) {
			SegPerfil perfil=asig.getSegPerfil();
			loginDTO.getListaPerfiles().add(perfil);
		}
		return loginDTO;
    }
    
    public void cerrarSesion(int idSegUsuario) {
    	mAuditoria.mostrarLog(getClass(), "cerrarSesion", "Cerrar sesión usuario: "+idSegUsuario);
    }
    
    public void accesoNoPermitido(int idSegUsuario,String ruta) {
    	mAuditoria.mostrarLog(getClass(), "accesoNoPermitido", "Usuario "+idSegUsuario+" intento no autorizado a "+ruta);
    }
    
    @SuppressWarnings("unchecked")
	public List<SegUsuario> findAllUsuarios(){
    	return mDAO.findAll(SegUsuario.class, "apellidos");
    }
    
    public SegUsuario findByIdSegUsuario(int idSegUsuario) throws Exception {
    	return (SegUsuario) mDAO.findById(SegUsuario.class, idSegUsuario);
    }
    
    public void insertarUsuario(SegUsuario nuevoUsuario) throws Exception {
    	nuevoUsuario.setCodigo("n/a");
    	mDAO.insertar(nuevoUsuario);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void actualizarUsuario(LoginDTO loginDTO,SegUsuario edicionUsuario) throws Exception {
    	SegUsuario usuario=(SegUsuario) mDAO.findById(SegUsuario.class, edicionUsuario.getIdSegUsuario());
    	usuario.setApellidos(edicionUsuario.getApellidos());
    	usuario.setClave(edicionUsuario.getClave());
    	usuario.setCorreo(edicionUsuario.getCorreo());
    	usuario.setCodigo(edicionUsuario.getCodigo());
    	usuario.setNombres(edicionUsuario.getNombres());
    	mDAO.actualizar(usuario);
    	mAuditoria.mostrarLog(loginDTO, getClass(), "actualizarUsuario", "se actualizó al usuario "+usuario.getApellidos());
    }
    
    public void activarDesactivarUsuario(int idSegUsuario) throws Exception {
    	SegUsuario usuario=(SegUsuario) mDAO.findById(SegUsuario.class, idSegUsuario);
    	if(idSegUsuario==1)
    		throw new Exception("No se puede desactivar al usuario administrador.");
    	usuario.setActivo(!usuario.getActivo());
    	System.out.println("activar/desactivar "+usuario.getActivo());
    	mDAO.actualizar(usuario);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void eliminarUsuario(int idSegUsuario) throws Exception {
    	SegUsuario usuario=(SegUsuario) mDAO.findById(SegUsuario.class, idSegUsuario);
    	if(usuario.getIdSegUsuario()==1)
    		throw new Exception("No se puede eliminar el usuario administrador.");
    	if(usuario.getSegAsignacions().size()>0)
    		throw new Exception("No se puede elimininar el usuario porque tiene asignaciones de módulos.");
    	mDAO.eliminar(SegUsuario.class, usuario.getIdSegUsuario());
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    @SuppressWarnings("unchecked")
	public List<SegModulo> findAllModulos(){
    	return mDAO.findAll(SegModulo.class, "nombreModulo");
    }
    
    public SegModulo insertarModulo(SegModulo nuevoModulo) throws Exception {
    	SegModulo modulo=new SegModulo();
    	modulo.setNombreModulo(nuevoModulo.getNombreModulo());
    	modulo.setIcono(nuevoModulo.getIcono());
    	mDAO.insertar(modulo);
    	return modulo;
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void eliminarModulo(int idSegModulo) throws Exception {
    	SegModulo modulo=(SegModulo) mDAO.findById(SegModulo.class, idSegModulo);
    	if(modulo.getSegPerfils().size()>0)
    		throw new Exception("No se puede eliminar un módulo que tiene perfiles asignados.");
    	mDAO.eliminar(SegModulo.class, idSegModulo);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void actualizarModulo(SegModulo edicionModulo) throws Exception {
    	SegModulo modulo=(SegModulo) mDAO.findById(SegModulo.class, edicionModulo.getIdSegModulo());
    	modulo.setNombreModulo(edicionModulo.getNombreModulo());
    	modulo.setIcono(edicionModulo.getIcono());
    	mDAO.actualizar(modulo);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    @SuppressWarnings("unchecked")
	public List<SegAsignacion> findAsignacionByUsuario(int idSegUsuario){
    	String consulta="o.segUsuario.idSegUsuario="+idSegUsuario;
		List<SegAsignacion> listaAsignaciones=mDAO.findWhere(SegAsignacion.class, consulta, "o.segPerfil.segModulo.nombreModulo,o.segPerfil.nombrePerfil");
		return listaAsignaciones;
    }
    
    /**
     * Permite asignar el acceso a un perfil del inventario de sistemas.
     * @param idSegUsuario El codigo del usuario.
     * @param idSegPerfil El codigo del perfil que se va a asignar.
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	public void asignarPerfil(int idSegUsuario,int idSegPerfil) throws Exception{
    	//Buscar los objetos primarios:
    	SegUsuario usuario=(SegUsuario)mDAO.findById(SegUsuario.class, idSegUsuario);
    	SegPerfil perfil=(SegPerfil)mDAO.findById(SegPerfil.class, idSegPerfil);
    	//verificar si ya existe:
    	String consulta="o.segPerfil.idSegPerfil="+idSegPerfil+" and o.segUsuario.idSegUsuario="+idSegUsuario;
    	List<SegAsignacion> asignaciones=mDAO.findWhere(SegAsignacion.class, consulta, null);
    	if(asignaciones==null || asignaciones.size()==0) {
	    	//crear la relacion:
	    	SegAsignacion asignacion=new SegAsignacion();
	    	asignacion.setSegPerfil(perfil);
	    	asignacion.setSegUsuario(usuario);
	    	//guardar la asignacion:
	    	mDAO.insertar(asignacion);
	    	mAuditoria.mostrarLog(getClass(), "asignarPerfil", "Perfil "+idSegPerfil+" asignado a usuario "+idSegUsuario);
    	}else {
    		throw new Exception("Ya existe la asignación de usuario/perfil ("+idSegUsuario+"/"+idSegPerfil+")");
    	}
    }
    
    public void eliminarAsignacion(int idSegAsignacion) throws Exception {
    	mDAO.eliminar(SegAsignacion.class, idSegAsignacion);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    @SuppressWarnings("unchecked")
	public List<SegPerfil> findPerfilesByModulo(int idSegModulo){
    	List<SegPerfil> listado=mDAO.findWhere(SegPerfil.class, "o.segModulo.idSegModulo="+idSegModulo, "o.nombrePerfil");
    	return listado;
    }
    
    public SegPerfil insertarPerfil(SegPerfil nuevoPerfil) throws Exception {
    	SegPerfil perfil=new SegPerfil();
    	perfil.setNombrePerfil(nuevoPerfil.getNombrePerfil());
    	perfil.setRutaAcceso(nuevoPerfil.getRutaAcceso());
    	perfil.setSegModulo(nuevoPerfil.getSegModulo());
    	mDAO.insertar(perfil);
    	return perfil;
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void eliminarPerfil(int idSegPerfil) throws Exception {
    	mDAO.eliminar(SegPerfil.class, idSegPerfil);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }
    
    public void actualizarPerfil(SegPerfil edicionPerfil) throws Exception {
    	SegPerfil perfil=(SegPerfil) mDAO.findById(SegPerfil.class, edicionPerfil.getIdSegPerfil());
    	perfil.setNombrePerfil(edicionPerfil.getNombrePerfil());
    	perfil.setRutaAcceso(edicionPerfil.getRutaAcceso());
    	mDAO.actualizar(perfil);
    	//TODO agregar uso de LoginDTO para auditar metodo.
    }

}

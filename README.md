# mmarketdemo_segsw

Software minimarket demo - ERP caso de estudio para implementar buenas prácticas de seguridad en software.
Core de sistema ERP con arquitectura monolítica multicapa JavaEE
Autor: mrea@utn.edu.ec

Plataforma de desarrollo:

- JavaEE
- IDE Eclipse
- Wildfly
- Postgresql

## Getting started

Tomar en cuenta las siguientes consideraciones para importar el proyecto en Eclipse:

- Crear la base de datos mmarketdemo en el gestor Postgresql con el script sql ubicado en mmarketdemoModel/docs.
- Crear un datasource en Wildfly denominado mmarketdemoDS.
- Crear un datasource en el IDE Eclipse denominado mmarketdemoDS.

## License
GNU GENERAL PUBLIC LICENSE Version 3
